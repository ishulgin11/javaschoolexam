package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.BiFunction;

public class Calculator {

    private static Map<Character, BiFunction<Double, Double, Double>> binaryOperatorMap;
    private static Map<Character, Integer> priorityMap;

    static {
        binaryOperatorMap = new HashMap<>();

        binaryOperatorMap.put('+', (a, b) -> a + b);
        binaryOperatorMap.put('-', (a, b) -> a - b);
        binaryOperatorMap.put('*', (a, b) -> a * b);
        binaryOperatorMap.put('/', (a, b) -> a / b);

        priorityMap = new HashMap<>();

        priorityMap.put('+', 0);
        priorityMap.put('-', 0);
        priorityMap.put('*', 1);
        priorityMap.put('/', 1);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals("")) {
            return null;
        }

        Stack<LinkedList<Double>> operands = new Stack<>();
        Stack<LinkedList<Character>> operators = new Stack<>();

        operands.push(new LinkedList<>());
        operators.push(new LinkedList<>());

        StringBuilder buffer = new StringBuilder();

        CharacterType previousCharacter = CharacterType.X;

        int parenthesisCounter = 0;

        for (char c : statement.toCharArray()) {
            switch (c) {
                case '(': {
                    if (!validateOpenParenthesis(previousCharacter)) {
                        return null;
                    }

                    operators.push(new LinkedList<>());
                    operands.push(new LinkedList<>());

                    parenthesisCounter++;

                    previousCharacter = CharacterType.OPEN_PARENTHESIS;

                    break;
                }
                case ')': {
                    if (!validateCloseParenthesis(previousCharacter)) {
                        return null;
                    }

                    parenthesisCounter--;

                    if (parenthesisCounter < 0) {
                        return null;
                    }

                    updateOperands(operands.peek(), buffer);

                    double temp = calculate(operands.pop(), operators.pop());

                    operands.peek().addLast(temp);

                    previousCharacter = CharacterType.CLOSE_PARENTHESIS;

                    break;
                }
                default: {
                    if (Character.isDigit(c)) {
                        if (!validateDigit(previousCharacter)) {
                            return null;
                        }

                        buffer.append(c);

                        previousCharacter = CharacterType.DIGIT;
                    } else if (c == '.') {
                        if (!validateDelimiter(previousCharacter)) {
                            return null;
                        }

                        buffer.append(c);

                        previousCharacter = CharacterType.DELIMITER;
                    } else if (c == '-' && validateMinus(previousCharacter)) {
                        buffer.append(c);

                        previousCharacter = CharacterType.UNARY_OPERATOR;
                    } else if (binaryOperatorMap.containsKey(c)) {
                        if (!validateBinaryOperator(previousCharacter)) {
                            return null;
                        }

                        if (previousCharacter == CharacterType.DIGIT) {
                            try {
                                operands.peek().addLast(Double.parseDouble(buffer.toString()));
                                buffer.setLength(0);
                            } catch (NumberFormatException e) {
                                return null;
                            }
                        }

                        operators.peek().addLast(c);

                        previousCharacter = CharacterType.BINARY_OPERATOR;
                    } else {
                        return null;
                    }

                    break;
                }
            }
        }

        if (parenthesisCounter > 0) {
            return null;
        }

        updateOperands(operands.peek(), buffer);

        return format(calculate(operands.peek(), operators.peek()));
    }

    private void updateOperands(LinkedList<Double> operands, StringBuilder buffer) {
        if (buffer.length() != 0) {
            operands.addLast(Double.parseDouble(buffer.toString()));
            buffer.setLength(0);
        }
    }

    private boolean validateOpenParenthesis(CharacterType previousCharacter) {
        return previousCharacter == CharacterType.X ||
                previousCharacter == CharacterType.BINARY_OPERATOR ||
                previousCharacter == CharacterType.OPEN_PARENTHESIS;
    }

    private boolean validateCloseParenthesis(CharacterType previousCharacter) {
        return previousCharacter == CharacterType.DIGIT || previousCharacter == CharacterType.CLOSE_PARENTHESIS;
    }

    private boolean validateDigit(CharacterType previousCharacter) {
        return previousCharacter != CharacterType.CLOSE_PARENTHESIS;
    }

    private boolean validateDelimiter(CharacterType previousCharacter) {
        return previousCharacter == CharacterType.DIGIT;
    }

    private boolean validateMinus(CharacterType previousCharacter) {
        return previousCharacter == CharacterType.X || previousCharacter == CharacterType.OPEN_PARENTHESIS;
    }

    private boolean validateBinaryOperator(CharacterType previousCharacter) {
        return previousCharacter == CharacterType.DIGIT || previousCharacter == CharacterType.CLOSE_PARENTHESIS;
    }

    private double calculate(LinkedList<Double> operands, LinkedList<Character> operators) {
        if (operands.size() != 1) {
            int operatorIndex = getHighestPriorityOperatorIndex(operators);

            char operator = operators.remove(operatorIndex);

            double a = operands.remove(operatorIndex);
            double b = operands.remove(operatorIndex);

            if (operands.size() == 0) {
                return binaryOperatorMap.get(operator).apply(a, b);
            }

            operands.add(operatorIndex, binaryOperatorMap.get(operator).apply(a, b));

            return calculate(operands, operators);
        }

        return operands.getFirst();
    }

    private int getHighestPriorityOperatorIndex(LinkedList<Character> operators) {
        int index = 0;
        char max = operators.getFirst();

        Iterator iterator = operators.iterator();

        for (int i = 0; i < operators.size(); i++) {
            char cur = (char) iterator.next();

            if (priorityMap.get(cur) > priorityMap.get(max)) {
                index = i;
                max = cur;
            }
        }

        return index;
    }

    private String format(double a) {
        if (Double.isNaN(a) || Double.isInfinite(a)) {
            return null;
        }

        return new BigDecimal(a).setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString();
    }
}