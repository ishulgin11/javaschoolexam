package com.tsystems.javaschool.tasks.calculator;

public enum CharacterType {
    DELIMITER,
    DIGIT,
    OPEN_PARENTHESIS,
    CLOSE_PARENTHESIS,
    UNARY_OPERATOR,
    BINARY_OPERATOR,
    X
}
