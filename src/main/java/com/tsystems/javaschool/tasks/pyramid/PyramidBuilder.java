package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException();

        int n = (int) (Math.sqrt(1 + 8 * inputNumbers.size()) - 1) / 2;

        if (inputNumbers.size() != n * (n + 1) / 2)
            throw new CannotBuildPyramidException();

        int[][] array = new int[n][2 * n - 1];

        List<Integer> sortedNumbers = inputNumbers.stream().sorted().collect(Collectors.toList());

        int currentRow = 1;
        int offset = 0;

        for (Integer sortedNumber : sortedNumbers) {
            array[currentRow - 1][n - currentRow + 2 * offset] = sortedNumber;

            offset++;

            if (offset == currentRow) {
                offset = 0;
                currentRow++;
            }
        }

        return array;
    }


}
